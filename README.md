# 蓝星云PHP框架1.0.7测试版（制作大版本中）

#### 介绍
由StarBit蓝星云团队编写的一个轻量的基于PHP的服务器/主机管理面板，内含管理面板、工具类和cms

#### 演示站
http://coco.codekpy.site/php/admin/index.php
<br>秘钥starbitphpsystem ##### 目前支持在安装页面设置密码了
#### 软件架构
软件架构说明
./install/install.php是安装文件<br>
./admin 是管理面板的源码<br>
./product 是蓝星云产品的一部分源码<br>


#### 安装教程

1.  访问./install/install.php
2.  填写全部信息
3.  点击安装

#### 使用说明

1.  访问./admin，并输入安装时设置的“面板管理密码”
2.  设置网站基本信息
3.  若新建站点，则站点目录在./站点名

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
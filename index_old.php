<?php 
	$webid="index";
    require("./config.php");//配置
    require("./function/function.php");//引入工具类
	require("./function/safe.php");//引入站点管理器
    //require("./index_html.php");//引入页面
	$name_file = fopen("./config/website_name.txt", "r") or die("无法读取网站名设置!");
    $websitename =  fread($name_file,filesize("./config/website_name.txt"));
    fclose($name_file);
	$text_file = fopen("./config/website_text.txt", "r") or die("无法读取网站介绍设置!");
    $websitetext =  fread($text_file,filesize("./config/website_text.txt"));
    fclose($text_file);
    $nav_file = fopen("./config/website_nav.txt", "r") or die("无法读取网站导航栏设置!");
    $websitenav =  fread($nav_file,filesize("./config/website_nav.txt"));
    fclose($nav_file);
?>
<html>
	<head>
		<title><?php echo $websitename ?></title>
		<style>
            .nav ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: rgb(91, 209, 255);
            }
            .nav li {
                float: left;
            }
            .nav li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }
            .nav li a:hover {
                background-color: rgb(158, 227, 255);
            }
		</style>
	</head>
	<body>
		<div class="nav">
            <ul>
                <li><a class="active" href="./index.php"><?php echo $websitename ?></a></li>
                <?php
                    echo $websitenav;
                ?>
            </ul>
		</div>
        <?php
            if($_GET["mode"]=="article"){
                require("./html/article.html");
            }elseif($_GET["mode"]=="talk"){
                require("./html/talk.html");
            }else{
                require("./html/index.html");
            }
        ?>
	</body>
</html>
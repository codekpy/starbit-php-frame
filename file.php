<?php
    if (isset($_GET["key"])&&$_GET["key"]==$token_pass){
        setcookie("token",md5($token_pass),time()+3600);
        echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";
    }else{

    }

?>
<html>
<head>
    <meta charset="UTF-8">
    <title>文件管理器</title>
    <style>
	body{ font-size:12px;}
	img{ width:20px;}
	a:link, a:visited { color:#555; text-decoration:none; } 
    a:hover, a:active { color:#ff0000; text-decoration:underline;} 
	.tbl{ border-collapse:collapse; width:98%; margin-top:10px; font-size:12px;}
	.tbl th{ text-align:left; background:#ccc;}
	.tbl th,td{ border:1px solid #eee;}
	span{color:#ff0000;}
  </style>
  <script src="./js/write.js"></script>
  <script src="./js/jQuery.min.js"></script>
</head>
<body>
<div id="file" style="background-color: #F7F7F7;width:100%;height: 100%;float: right;overflow:auto;">
<div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">文件管理器</h3></div>
<div id="table">
<script>function new_file(){window.location.replace('?a=newfile')}</script>
<script>function new_dir(){window.location.replace('?a=newdir')}</script>
<script>function on_file(){window.location.replace('?a=onfile')}</script>
<div><button style="float:left" onclick='new_file()'> 新建文件</button><button style="float:left" onclick='new_dir()'> 新建目录</button><button style="float:left" onclick='on_file()'> 上传文件</button></div>
<?php
//case 26 文件管理器
//设置配置文件中，只能访问本目录
ini_set('open_basedir',__DIR__);
//路径
$path = isset($_GET['path'])?$_GET['path']:'.';
//文件名
//判断，如果是文件类型
if(is_file($path)){
    //获得文件名
    $file = basename($path);
    //获得路径
    $path = dirname($path);
}elseif(!is_dir($path)){//判断，不是目录
    die('无效的文件路径');
}
 
//获得文件列表
function getFileList($path){
    //打开目录，获得句柄
    $handle = opendir($path);
    //空数组
    $list = array('dir'=>array(),'file'=>array());
    //从目录总读取文件
    while(false !==($file_name = readdir($handle))){
        //除去上级目录和本级目录
        if($file_name != '.' && $file_name != '..'){
            //文件全路径
            $file_path = "$path/$file_name";
            //文件类型
            $file_type = filetype($file_path);
            //判断，文件类型是文件或者目录
            if(!in_array($file_type,array('file','dir'))){
                continue;
            }
            //数组填入值
            $list[$file_type][] = array(
                'file_name'=>$file_name,
                'file_path'=>$file_path,
                'file_size'=>round(filesize($file_path)/1024),
                'file_time'=>date('Y/m/d H:i:s',filemtime($file_path)),
            );
        }
    }
    //释放句柄
    closedir($handle);
    return $list;
}
//处理操作
$action = isset($_GET['a'])?$_GET['a']:'';
//根据操作动作，执行相应程序
if(isset($_POST["newfile"])){
    $myfile = fopen($path."/".$_POST["file_name"], "w") or die("Unable to open file!");
    fwrite($myfile, "");
    fclose($myfile);
    header('Location:?path='.$path);
    die;
}elseif(isset($_POST["newdir"])){
    mkdir($path."/".$_POST["dir_name"]);
    header('Location:?path='.$path);
    die;
}elseif(isset($_POST["onfile"])){

}else{
    switch ($action){
        //返回上一级
        case 'prev':
            $path = dirname($path);
            break;
        //复制
        case 'copy':
            if($file)
            {
                if(file_exists("$path/$file.txt"))
                {
                    die('文件名冲突，复制失败');
                }
                if(!copy("$path/$file","$path/$file.txt"))
                {
                    die('复制文件失败');
                }
            }
            break;
        //删除
        case 'del':
            if($file)
            {
                unlink("$path/$file");
            }
            break;
        //重命名
        case 'rename':
            if(isset($_POST['rename']))
            {
                $target = isset($_POST['target'])?trim($_POST['target']):'';
                if($file && $target)
                {
                    if(file_exists("$path/$target"))
                    {
                        die('目标文件已存在');
                    }
                    rename("$path/$file","$path/$target");
                    header('Location:?path='.$path);
                    die;
                }
     
            }
            break;
    
    }
}

$file_list = getFileList($path);
?>
 

<div>
    <a href="?path=<?= $path;?>&a=prev">返回上一级目录</a>
</div>
<?php  if($action == 'rename'): ?>
<form method="post">
    将<span><?= $file;?></span>
    重命名为：<input type="text" value="<?= $file;?>" name="target">
    <input name="pa" value="7" style="display:none">
    <input type="submit" name="rename" value="确定">
</form>
<?php endif;?>
<?php  if($action == 'newfile'): ?>
<form method="post">
    新建文件<br>
    文件名为：<input type="text" value="" name="file_name">
    <input name="pa" value="7" style="display:none">
    <input type="submit" name="newfile" value="确定">
</form>
<?php endif;?>
<?php  if($action == 'newdir'): ?>
<form method="post">
    新建目录<br>
        目录名为：<input type="text" value="" name="dir_name">
        <input name="pa" value="7" style="display:none">
        <input type="submit" name="newdir" value="确定">
</form>
<?php endif;?>
<?php  if($action == 'onfile'): ?>
<form method="post">
    上传文件<br>
        <input type="file" value="" name="file">
        <input name="pa" value="7" style="display:none">
        <input type="submit" name="rename" value="确定">
</form>
<?php endif;?>
 
<table width="60%" style="font-size: 9px;text-align: center;">
    <tr>
        <th>图标</th>
        <th>名称</th>
        <th>修改日期</th>
        <th>大小</th>
        <th>路径</th>
        <th>操作</th>
    </tr>
    <?php  foreach ($file_list['dir'] as $v): ?>
    <tr>
        <td><img src="./admin/img/list.svg"></td>
        <td><?= $v['file_name'];?></td>
        <td><?= $v['file_time']?></td>
        <td>-</td>
        <td><?= $v['file_path'];?></td>
        <td><a href="?path=<?= $v['file_path'];?>">打开</a></td>
    </tr>
    <?php endforeach;?>
    <?php foreach ($file_list['file'] as $v):?>
    <tr>
        <td><img src="./admin/img/file.svg"></td>
        <td><a href="<?= $v['file_path'];?>"><?= $v['file_name'];?></a></td>
        <td><?= $v['file_time'];?></td>
        <td><?= $v['file_size'];?>KB</td>
        <td><?= $v['file_path'];?></td>
        <td>
           <a href="?path=<?= $v['file_path']?>&a=rename">重命名</a>&nbsp;&nbsp;
           <p onclick="write('<?php echo $v['file_path'] ?>')">编辑</p>&nbsp;&nbsp;
            <a href="?path=<?= $v['file_path']?>&a=copy">复制</a>&nbsp;&nbsp;
            <a href="?path=<?= $v['file_path']?>&a=del">删除</a></td>
    </tr>
    <?php endforeach;?>
</table>
</div>
<div id="write_text" style="display:none">
    <img src="./admin/img/close.svg">
    <textarea id="file_code" rows="15" style="width:80%"></textarea>
    <p style="color:yellow"><img src="./admin/img/close.svg">保存</p>
</div>
</div>
</body>
</html>

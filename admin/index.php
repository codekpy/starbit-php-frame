<html>
    <head>
      <meta charset="UTF-8">
      <title>站点管理面板</title>
      <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
      <script src="../js/jQuery.min.js"></script>
      <head>
    <style>
        .left_button{
            width: 100%;
            height: 60px;
            background-color: #696969;
        }
        .left_button:hover{
            width: 100%;
            height: 60px;
            background-color: #A7A7A7;
        }
        .left_button p{
            padding: 10% 5% 10% 10%;
            color: white
        }
    </style>
</head>
    </head>
    <body>

    <?php
    //if (file_exists("../config.php")){
       // exit("<script>window.location.replace(\"../install/install.php\")</script>");
    //}
    require("../config.php");

    if (isset($_GET["key"])&&$_GET["key"]==$token_pass){
        setcookie("token",md5($token_pass),time()+3600);
        echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";
    }else{
        $con=mysqli_connect($servername,$username,$password,$dbname);
        $status=1;
        if(isset($_COOKIE["token"])){
            
            if($_COOKIE["token"]==md5($token_pass)){
                if (isset($_GET["pa"])){
                
                }else{
                    echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";
                }
                //管理界面
                require("./nav.html");

                if($_GET["mode"]=="off"){//关闭站点
                    mysqli_query($con,"UPDATE `star_php_website` SET `status`='0' WHERE id='".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";

                }elseif($_GET["mode"]=="open"){//开启站点
                    mysqli_query($con,"UPDATE `star_php_website` SET `status`='1' WHERE id='".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";

                }elseif($_GET["mode"]=="del"){//删除站点
                    mysqli_query($con,"DELETE FROM `star_php_website` WHERE `star_php_website`.`id` = '".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php?pa=1\")</script>";
                }elseif($_GET["mode"]=="new_site"){//新建站点

                    $result_websiteinfo = mysqli_query($con,"SELECT * FROM star_php_website WHERE name='".$_GET["name"]."'");
                    if($result_websiteinfo->num_rows == 0){
                        mkdir("../".$_GET["name"]);
                        $id = hash("sha256", time().$_GET["name"]);
                        $name = $_GET["name"];
                        $test = mysqli_query($con, "INSERT INTO `star_php_website` (`id`, `name`, `status`) VALUES ('$id', '$name', '1');");
                        $webid = '$webid';
                        if($_GET["mode_site"]=="no"){//默认框架
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile,"<?php $webid='$id'; require('../config.php'); require('../function/function.php'); require('../function/safe.php'); ?>\n<!--请在下面写您的代码——蓝星云PHP框架的开发者>");
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
                        }elseif($_GET["mode_site"]=="code"){//验证系统
                            $safe_ep = fopen("../product/code/index.php", "r") or die("Unable to open file!");
                            $main_php = fread($safe_ep,filesize("../product/code/index.php"));
                            fclose($safe_ep);
                            echo $main_php;
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile,"<?php $webid='$id'; ".$main_php);
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
                        }elseif($_GET["mode_site"]=="email"){
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile, "<?php $webid='$id'; require('../config.php'); require('../function/function.php'); require('../function/safe.php'); ?>\n<!--请在下面写您的代码——蓝星云PHP框架的开发者><?php sendMail(".'$smtpurl, $smtpemail, $smtpport, $smtpkey, $to,$name,$title,$content'."); ?>");
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
                        }
                    }else{
                        exit("站点已存在，请换一个名字<a href='./index.php'>返回</a>");
                    }
                    
                }elseif($_GET["mode"]=="system_set"){
                    $setname_file = fopen("../config/website_name.txt", "w") or die("Unable to open file!");
                    fwrite($setname_file,$_GET["website_name"]);
                    fclose($setname_file);
                    $settext_file = fopen("../config/website_text.txt", "w") or die("Unable to open file!");
                    fwrite($settext_file,$_GET["website_text"]);
                    fclose($settext_file);
                    $setnav_file = fopen("../config/website_nav.txt", "w") or die("Unable to open file!");
                    fwrite($setnav_file,$_GET["website_nav"]);
                    fclose($setnav_file);
                    $setconfig_file = fopen("../config.php", "w") or die("Unable to open file!");
                    fwrite($setconfig_file,$_GET["website_config"]);
                    fclose($setconfig_file);

                    exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
                }elseif($_GET["mode"]=="new_token"){
                    $id = md5(time());
                    $time = $_GET["time"];
                    mysqli_query($con, "INSERT INTO `star_php_token` (`id`, `time`, `status`) VALUES ('$id', '$time', '1');");
                    exit("<script>window.location.replace(\"./index.php?pa=3\")</script>");
                }elseif($_GET["mode"]=="del_token"){
                    $result = mysqli_query($con,"SELECT * FROM star_php_token WHERE id='".$_GET["id"]."'");
                    if($result->num_rows == 0){
                        exit("<script>window.location.replace(\"./index.php?pa=3\")</script>");
                    }else{
                        mysqli_query($con,"DELETE FROM `star_php_token` WHERE `star_php_token`.`id` = '".$_GET["id"]."'");
                    }
                }
                if($_GET["pa"]=="1"){
                    require("./website.php");
                }elseif($_GET["pa"]=="2"){
                    $name_file = fopen("../config/website_name.txt", "r") or die("Unable to open file1!");
                    $websitename =  fread($name_file,filesize("../config/website_name.txt"));
                    fclose($name_file);
                    $text_file = fopen("../config/website_text.txt", "r") or die("Unable to open file2!");
                    $websitetext =  fread($text_file,filesize("../config/website_text.txt"));
                    fclose($text_file);
                    $nav_file = fopen("../config/website_nav.txt", "r") or die("无法读取网站导航栏设置!");
                    $websitenav =  fread($nav_file,filesize("../config/website_nav.txt"));
                    fclose($nav_file);
                    $config_file = fopen("../config.php", "r") or die("无法读取网站导航栏设置!");
                    $websiteconfig =  fread($config_file,filesize("../config.php"));
                    fclose($config_file);
                    require("./system.php");
                }elseif($_GET["pa"]=="3"){
                    require("./token.php");
                }elseif($_GET["pa"]=="4"){
                    require("./product.php");
                }elseif($_GET["pa"]=="5"){
                    require("./aboutus.php");
                }elseif($_GET["pa"]=="7"){
                    require("./file.php");
                }elseif($_GET["pa"]=="8"){
                    exit("<script>window.open(\"../function/md/examples/\")</script>");
                }elseif($_GET["pa"]=="6"){
                    require("./cms-acticle.php");
                }elseif($_GET["pa"]=="9"){
                exit("<script>window.open(\"./db.php\")</script>");
                }elseif($_GET["pa"]=="10"){
                    require("./back.php");
                }
            }else{
                echo "<h3>蓝星云PHP框架1.0.5版本</h3><form method='get'>密钥：<input name='key'><input type='submit' value='登录'></form>";
            }
        }else{
            echo "<h3>蓝星云PHP框架1.0.5版本</h3><form method='get'>密钥：<input name='key'><input type='submit' value='登录'></form>";
        }
    }
    ?>
    
    </body>
</html>
<?php
if($status!=1){
    exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
}else{

}
?>
<div id="about_us" style="background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">关于我们</h3></div>
    <h3>关于我们</h3>
    <div>由StarBit蓝星云团队编写的一个轻量的PHP框架，内含管理面板、工具类</div>
    <h3>致谢名单</h3>
    <p>CodeKpy(前后端开发)</p>
    <p>静静地看星空（新·官网前端设计）</p>
    <p>鹈（原始官网前端设计）</p>
    <p>Gitee(托管代码源码)</p>
    <p>恶狼云(虚拟主机商)</p>
    <p>Editor.md（MarkDown编辑器）</p>
    <p>Adminer（数据库管理器）</p>
    <h3>贡献项目</h3>
    <p>仓库地址：<a href='https://gitee.com/codekpy/starbit-php-frame'>https://gitee.com/codekpy/starbit-php-frame</a></p>
    <p>提建议：向1942171924@qq.com发送邮件</p>
    <h3>征集</h3>
    <p>目前正在收集封装成函数的工具类，您可以向1942171924@qq.com发送邮件</p>
</div>
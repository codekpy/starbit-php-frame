<?php
if(!isset($_GET["pa"])){
    exit("<script>window.location.replace(\"./index.php?pa=1\")</script>");
}
?>
<head>
    <style>
        .left_button{
            width: 100%;
            height: 60px;
            background-color: #696969;
        }
        .left_button:hover{
            width: 100%;
            height: 60px;
            background-color: #A7A7A7;
        }
        .left_button p{
            padding: 10% 5% 10% 10%;
            color: white
        }
    </style>
    <script src="../js/jQuery.min.js"></script>
</head>
<div style="width: 15%;height: 100%;background-color: grey;float: left;">
    <div class="left_button" onclick="window.location.replace('./index.php?pa=1');"><p>站点管理</p></div>
    <div class="left_button" onclick="window.location.replace('./index.php?pa=2');"><p>系统设置</p></div>
    <div class="left_button" onclick=""><p>蓝星云CMS</p></div>
    <div class="left_button" onclick="window.location.replace('./index.php?pa=5')"><p>临时授权管理</p></div>
    <div class="left_button" onclick="window.location.replace('./index.php?pa=3');"><p>蓝星云产品</p></div>
    <div class="left_button" onclick="$('#about_us').show();$('#website').hide();$('#product').hide();$('#system').hide();$('#token').hide()"><p>关于我们</p></div>
</div>
<div id="token" style="display:none;background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">面板授权</h3></div>
    <p style="color:red">*请保管好临时授权管理面板的网址，不要在公共平台泄露</p>
    <table border="1">
    <tr>
            <td>临时授权管理面板网址</td>
            <td>授权ID</td>
            <td>有效时间</td>
            <td>是否有效</td>
            <td>操作</td>
    </tr>
    <?php 
    if($_GET["pa"]=="5"){

    }
    ?>
    </table>
    <button onclick='$("#off_token").show();$("#make_token").hide();$("#new_token_div").show();' id='make_token' style='background-color:green;color:white;'>新建授权</button>
    <button onclick='$("#off_token").hide();$("#make_token").show();$("#new_token_div").hide();' id='off_token' style='display:none;background-color:green;color:white;'>取消</button>
    <form action="./index.php" method="get" style="display: none;" id="new_token_div">
        <p>请输入有效时间（分钟）:<input name="time" type="text"></p>
        <input name="mode" value="new_token" style="display: none;">
        <input value="确定" type="submit" name="new_token_div">
    </from>
</div>
<div id="website" style="background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">管理站点</h3></div>
    <table border="1">
        <tr>
            <td>站点ID</td>
            <td>站点名</td>
            <td>运行状态（1为正常）</td>
        </tr>
        <?php
            if($status!=1){
                exit("<script>window.location.replace(\"./index.php\")</script>");
            }else{
                $name_file = fopen("../config/website_name.txt", "r") or die("Unable to open file1!");
                $websitename =  fread($name_file,filesize("../config/website_name.txt"));
                fclose($name_file);
                $text_file = fopen("../config/website_text.txt", "r") or die("Unable to open file2!");
                $websitetext =  fread($text_file,filesize("../config/website_text.txt"));
                fclose($text_file);
                $nav_file = fopen("../config/website_nav.txt", "r") or die("无法读取网站导航栏设置!");
                $websitenav =  fread($nav_file,filesize("../config/website_nav.txt"));
                fclose($nav_file);

                $con=mysqli_connect($servername,$username,$password,$dbname);
                if($_GET["mode"]=="off"){
                    mysqli_query($con,"UPDATE `star_php_website` SET `status`='0' WHERE id='".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php\")</script>";
                }elseif($_GET["mode"]=="open"){
                    mysqli_query($con,"UPDATE `star_php_website` SET `status`='1' WHERE id='".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php\")</script>";
                }elseif($_GET["mode"]=="del"){
                    mysqli_query($con,"DELETE FROM `star_php_website` WHERE `star_php_website`.`id` = '".$_GET["id"]."'");
                    echo "<script>window.location.replace(\"./index.php\")</script>";
                }elseif($_GET["mode"]=="new_site"){
                    $result_websiteinfo = mysqli_query($con,"SELECT * FROM star_php_website WHERE name='".$_GET["name"]."'");
                    if($result_websiteinfo->num_rows == 0){
                        mkdir("../".$_GET["name"]);
                        $id = hash("sha256", time().$_GET["name"]);
                        $name = $_GET["name"];
                        $test = mysqli_query($con, "INSERT INTO `star_php_website` (`id`, `name`, `status`) VALUES ('$id', '$name', '1');");
                        $webid = '$webid';
                        if($_GET["mode_site"]=="no"){//默认框架
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile,"<?php $webid='$id'; require('../config.php'); require('../function/function.php'); require('../function/safe.php'); ?>\n<!--请在下面写您的代码——蓝星云PHP框架的开发者>");
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php\")</script>");
                        }elseif($_GET["mode_site"]=="code"){//验证系统
                            $safe_ep = fopen("../product/code/index.php", "r") or die("Unable to open file!");
                            $main_php = fread($safe_ep,filesize("../product/code/index.php"));
                            fclose($safe_ep);
                            echo $main_php;
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile,"<?php $webid='$id'; ".$main_php);
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php\")</script>");
                        }elseif($_GET["mode_site"]=="email"){
                            $myfile = fopen("../".$_GET["name"]."/index.php", "w") or die("Unable to open file!");
                            fwrite($myfile, "<?php $webid='$id'; require('../config.php'); require('../function/function.php'); require('../function/safe.php'); ?>\n<!--请在下面写您的代码——蓝星云PHP框架的开发者><?php sendMail(".'$smtpurl, $smtpemail, $smtpport, $smtpkey, $to,$name,$title,$content'."); ?>");
                            fclose($myfile);
                            exit("<script>window.location.replace(\"./index.php\")</script>");
                        }
                    }else{
                        exit("站点已存在，请换一个名字<a href='./index.php'>返回</a>");
                    }  
                }elseif($_GET["mode"]=="system_set"){
                    $setname_file = fopen("../config/website_name.txt", "w") or die("Unable to open file!");
                    fwrite($setname_file,$_GET["website_name"]);
                    fclose($setname_file);
                    $settext_file = fopen("../config/website_text.txt", "w") or die("Unable to open file!");
                    fwrite($settext_file,$_GET["website_text"]);
                    fclose($settext_file);
                    $setnav_file = fopen("../config/website_nav.txt", "w") or die("Unable to open file!");
                    fwrite($setnav_file,$_GET["website_nav"]);
                    fclose($setnav_file);
                    exit("<script>window.location.replace(\"./index.php\")</script>");
                }elseif($_GET["mode"]=="new_token"){
                    $id = md5(time());
                    $time = $_GET["time"];
                    mysqli_query($con, "INSERT INTO `star_php_web_token` (`id`, `time`) VALUES ('$id', '$time');");
                    exit("<script>window.location.replace(\"./index.php?pa=5\")</script>");
                }else{
                    $result = mysqli_query($con,"SELECT * FROM star_php_website WHERE status='1'");
                    while($row = mysqli_fetch_array($result)){
                        echo "<tr><td>".$row['id']."</td><td>".$row["name"]."</td><td>".$row["status"]."<button onclick='window.location.replace(\"./index.php?mode=off&id=".$row["id"]."\")'>关闭</button><button onclick='window.location.replace(\"./index.php?mode=del&id=".$row["id"]."\")'>删除</button><p style='color: red;float:right;margin:0px;'>（不会删除文件）</p></td></tr>";
                    }
                    $result = mysqli_query($con,"SELECT * FROM star_php_website WHERE status='0'");
                    while($row = mysqli_fetch_array($result)){
                        echo "<tr><td>".$row['id']."</td><td>".$row["name"]."</td><td>".$row["status"]."<button onclick='window.location.replace(\"./index.php?mode=open&id=".$row["id"]."\")'>开启</button><button onclick='window.location.replace(\"./index.php?mode=del&id=".$row["id"]."\")'>删除</button><p style='color: red;float:right;margin:0px;'>（不会删除文件）</p></td></tr>";
                    }
                }
            }


        ?>
    </table>
    <br>
    <button onclick='$("#off_site").show();$("#make_website").hide();$("#new_site_div_f").show();' id='make_website' style='background-color:green;color:white;'>新建站点</button>
    <button onclick='$("#off_site").hide();$("#make_website").show();$("#new_site_div_f").hide();' id='off_site' style='display:none;background-color:green;color:white;'>取消</button>
    <form action="./index.php" method="get" style="display: none;" id="new_site_div_f">
        <p>请输入站点名（英文）:<input name="name" type="text"></p>
        <input name="mode" value="new_site" style="display: none;">
        <p>请选择部署模式：<input name="mode_site" type="radio" value="no" checked="checked">默认框架<input name="mode_site" type="radio" value="code">验证码框架<a href='../docs/code.html'>使用文档</a><input name="mode_site" type="radio" value="email">SMTP发件框架</p>
        <input type='submit'>
    </from>
</div>
<div id="product" style="background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">部署蓝星云产品</h3></div>
</div>
<div id="about_us" style="background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">关于我们</h3></div>
    <h3>关于我们</h3>
    <div>由StarBit蓝星云团队编写的一个轻量的PHP框架，内含管理面板、工具类</div>
    <h3>致谢名单</h3>
    <p>CodeKpy(前后端开发)</p>
    <p>Gitee(托管代码源码)</p>
    <p>恶狼云(虚拟主机商)</p>
    <h3>贡献项目</h3>
    <p>仓库地址：<a href='https://gitee.com/codekpy/starbit-php-frame'>https://gitee.com/codekpy/starbit-php-frame</a></p>
    <p>提建议：向1942171924@qq.com发送邮件</p>
    <h3>征集</h3>
    <p>目前正在收集封装成函数的工具类，您可以向1942171924@qq.com发送邮件</p>
</div>
<div id="system" style="background-color: #F7F7F7;width:85%;height: 100%;float: right;overflow:auto;">
    <div style="width: 100%;height: 50px;background-color: #B9FFD1;"><h3 style="padding: 10px 2%;margin: 0px;">系统设置</h3></div>
    <form action="./index.php" mode="get">
        <input name="pa" value="2" style="display:none">
        <input name="mode" value="system_set" style="display:none">
        <p>网站名：<input name="website_name" value="<?php echo $websitename; ?>"></p>
        <p>网站介绍：<input name="website_text" style="width:80%" value="<?php echo $websitetext; ?>"></p>
        <p>网站导航栏：</p>
        <textarea name="website_nav" rows="15" style="width:80%"><?php echo $websitenav; ?></textarea>
        <input type="submit" value="保存">
    </form>
</div>
<?php
    echo "<script>";
    if($_GET["pa"]=="1"){
        echo "$('#website').show();$('#product').hide();$('#about_us').hide();$('#system').hide();$('#token').hide();";
    }elseif($_GET["pa"]=="2"){
        echo "$('#system').show();$('#product').hide();$('#about_us').hide();$('#website').hide();$('#token').hide();";
    }elseif($_GET["pa"]=="3"){
        echo "$('#product').show();$('#website').hide();$('#about_us').hide();$('#system').hide();$('#token').hide();";
    }elseif($_GET["pa"]=="4"){
        echo "$('#about_us').show();$('#website').hide();$('#product').hide();$('#system').hide();$('#token').hide();";
    }elseif($_GET["pa"]=="5"){
        echo "$('#token').show();$('#website').hide();$('#product').hide();$('#system').hide();$('#about_us').hide();";
    }
    echo "</script>";
?>
<html>
    <head>
      <meta charset="UTF-8">
      <title>安装框架</title>
    </head>
    <body>
    <h2>欢迎使用StarBit的PHP框架v1.0.4</h2>
    <p>将安装以下组件</p>
    <?php require("./a.html"); ?>
    <form action="./install.php" method="get">
        <input style="display: none;"name="mode" value="install">
        <p>数据库地址：<input name="servername" type="text"></p>
        <p>数据库用户名：<input name="username" type="text"></p>
        <p>数据库库名：<input name="dbname" type="text"></p>
        <p>数据库密码：<input name="password" type="password"></p>
        <p>面板管理密码：<input name="token_pass" type="text"></p>
        <p>smtp地址：<input name="smtpurl" type="text"></p>
        <p>smtp发件邮箱：<input name="smtpemail" type="text"></p>
        <p>smtp端口：<input name="port" type="text"></p>
        <p>smtp授权码：<input name="smtppassword" type="password"></p>
        <p>告警信息接受邮箱：<input name="tellemail" type="email"></p>
        <input type="submit" value="安装">
    </form>
    </body>
</html>        
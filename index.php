<?php 
	$webid="index";
    require("./config.php");//配置
    require("./function/function.php");//引入工具类
	require("./function/safe.php");//引入站点管理器
    //require("./index_html.php");//引入页面
	$name_file = fopen("./config/website_name.txt", "r") or die("无法读取网站名设置!");
    $websitename =  fread($name_file,filesize("./config/website_name.txt"));
    fclose($name_file);
	$text_file = fopen("./config/website_text.txt", "r") or die("无法读取网站介绍设置!");
    $websitetext =  fread($text_file,filesize("./config/website_text.txt"));
    fclose($text_file);
    $nav_file = fopen("./config/website_nav.txt", "r") or die("无法读取网站导航栏设置!");
    $websitenav =  fread($nav_file,filesize("./config/website_nav.txt"));
    fclose($nav_file);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <script src="./js/flexible.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/common.css" />
    <link rel="stylesheet" type="text/css" href="./css/index.css" />
  </head>
  <body>
    <div class="page flex-col">
      <div class="block_1 flex-row">
        <span class="text_1">星点</span>
        <span class="text_2">首页</span>
        <span class="text_3">讨论</span>
        <div class="group_1 flex-col"></div>
      </div>
      <div class="block_2 flex-col">
        <div class="box_1 flex-row justify-between">
          <div class="group_2 flex-col"></div>
          <div class="text-wrapper_1 flex-col">
            <span class="text_4">星点邮箱项目</span>
            <span class="text_5">操作面板项目</span>
            <span class="text_6">XXX项目</span>
            <span class="text_7">XXX项目</span>
          </div>
        </div>
        <div class="box_2 flex-row">
          <div class="box_3 flex-col">
            <div class="group_3 flex-col">
              <div class="block_3 flex-col"></div>
            </div>
            <div class="text-group_1 flex-col justify-between">
              <span class="text_8">Codekpy</span>
              <span class="text_9">设计师、前端工程师、后端工程师</span>
            </div>
          </div>
          <div class="box_4 flex-col">
            <div class="group_4 flex-col">
              <div class="group_5 flex-col"></div>
            </div>
            <div class="text-group_2 flex-col justify-between">
              <span class="text_10">静静地看星空</span>
              <span class="text_11">设计师、前端工程师</span>
            </div>
          </div>
          <div class="box_5 flex-col">
            <div class="section_1 flex-col">
              <div class="box_6 flex-col"></div>
            </div>
            <div class="text-group_3 flex-col justify-between">
              <span class="text_12">XXXXX</span>
              <span class="text_13">设计师、前端工程师、后端工程师</span>
            </div>
          </div>
          <div class="box_7 flex-col">
            <div class="group_6 flex-col">
              <div class="block_4 flex-col"></div>
            </div>
            <div class="text-group_4 flex-col justify-between">
              <span class="text_14">XXXXX</span>
              <span class="text_15">设计师、前端工程师、后端工程师</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
